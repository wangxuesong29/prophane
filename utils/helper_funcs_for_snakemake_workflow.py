import hashlib
import yaml

from utils.db_handling import DbAccessor
from utils.db_handling.db_access import get_db_object_for_dbtype_and_version


def get_taxblast_result(config):
    if not config['taxblast']:
        return []
    return config['output']['task_result'].replace("{n}", str(config['taxblast']))


def get_md5_for_task(task_dict, db_base_dir):
    md5hash = hashlib.md5()
    md5hash.update(task_dict["prog"].encode())
    md5hash.update(task_dict["type"].encode())
    md5hash.update(task_dict["db_type"].encode())
    db_obj = get_db_obj_for_task_and_dbdir(task_dict, db_base_dir)
    md5hash.update(db_obj.get_version().encode())
    if "params" in task_dict:
        md5hash.update(yaml.dump(task_dict["params"], sort_keys=True).encode())
    return md5hash.hexdigest()


# import pytest
# def test_get_md5_for_task(mocker):
#     # mocker.patch(get_db_obj_for_task_and_dbdir, autospec=True)
#     dummy_dict = {"prog": "diamond", "type": "functional", "db_type": "ncbi_nr",
#                   "params": {"a": 3, "i": 0.01}}
#     print(get_md5_for_task(dummy_dict, "tut"))


def get_task_best_hits_file(taskid, task_dict, db_base_dir):
    task_file_base_string = get_task_file_base_string(db_base_dir, task_dict, taskid)
    return f"tasks/{task_file_base_string}.best_hits"


def get_task_lca(taskid, task_dict, db_base_dir):
    task_file_base_string = get_task_file_base_string(db_base_dir, task_dict, taskid)
    return f'tasks/{task_file_base_string}.lca'


def get_task_map(taskid, task_dict, db_base_dir):
    task_file_base_string = get_task_file_base_string(db_base_dir, task_dict, taskid)
    return f'tasks/{task_file_base_string}.map'


def get_task_plot_file(taskid, task_dict, db_base_dir):
    task_file_base_string = get_task_file_base_string(db_base_dir, task_dict, taskid)
    return f"plots/plot_of_{task_file_base_string}.html"


def get_task_file_base_string(db_base_dir, task_dict, taskid):
    annot_type = task_dict["type"][:3]
    db_type = task_dict["db_type"]
    prog = task_dict["prog"]
    if prog in ['hmmscan', 'hmmsearch']:
        if db_type == "foam":
            tool = "hmmer-3-0"
        else:
            tool = "hmmer-latest"
    elif prog == "diamond blastp":
        tool = "diamond"
    elif prog == "emapper":
        tool = "emapper"
    else:
        raise ValueError(f"Unexpected prog string in task {taskid}: {prog}")
    md5sum = get_md5_for_task(task_dict, db_base_dir)
    return f"{annot_type}_annot_by_{tool}_on_{db_type}.task{taskid}.{md5sum}"


def get_mafft_report(n):
    return f'algn/mafft.{n}.txt'


def get_db_obj_for_task_and_dbdir(task_config_dict, dbdir, db_acc=None):
    if not db_acc:
        db_acc = DbAccessor(dbdir)
    db_type = task_config_dict['db_type']
    db_version = 'newest'
    if 'db_version' in task_config_dict:
        db_version = task_config_dict['db_version']
    db_obj = get_db_object_for_dbtype_and_version(
        db_type,
        db_version,
        dbdir,
        db_acc
    )
    return db_obj
