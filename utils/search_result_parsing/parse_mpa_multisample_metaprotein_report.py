#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
input: file names of MPA's metaprotein report, style yaml and output
output: protein group yaml for prophane
"""

import pandas as pd
import sys

from utils.search_result_parsing.helper_classes import ProteomicSearchResultReaderBaseClass


class MpaPortableMultisampleTableReader(ProteomicSearchResultReaderBaseClass):
    mandatory_style_fields = ProteomicSearchResultReaderBaseClass.mandatory_style_fields[:]
    mandatory_style_fields.remove("sample_cols")
    mandatory_style_fields.remove('quant_col')

    def __init__(self, *args, **kwargs):
        super(MpaPortableMultisampleTableReader, self).__init__(*args, **kwargs)
        if len(self.required_sample_descriptors) == 0:
            print("Error: Got empty sample descriptors argument. Parsing of MPA MultiSample Results requires sample "
                  "column names", file=sys.stderr)
            sys.exit(1)

    def read_file_into_dataframe(self):
        required_col_names = [self.style_dict['proteins_col'], ]
        required_col_names += self.required_sample_descriptors

        raw_df = pd.read_csv(self.result_table, sep=self.style_dict['field_sep'])

        df = raw_df[required_col_names].copy()
        del raw_df
        df = pd.melt(df, id_vars=[self.style_dict['proteins_col']], value_vars=self.required_sample_descriptors,
                     var_name='sample', value_name='quant')
        df.rename(columns={self.style_dict['proteins_col']: "proteins"}, inplace=True)

        df['proteins'] = df['proteins'].apply(lambda x: x.split(self.style_dict['proteins_sep'])[:-1])

        return df
