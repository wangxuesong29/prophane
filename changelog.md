# Changelog

## 3.3.1
- fix: fail on non-defined task-parameters in config yaml

## 3.3.0
- Added new config key `search_result` in input section, which can be used in combination with all search result styles,
    making style-specific file keys obsolete
- Make `sample_groups` config section optional for input styles that allow it to reduce the number of mandatory 
    execution arguments  
  
  styles for which `sample_groups` is optional:
  - generic.yaml
  - mpa_1_8_x.yaml
  - scaffold_4_8_x.yaml
  
  styles for which `sample_groups` is mandatory:
  - mpa_server_multisample.yaml

## 3.2.0
- Added support for [FOAM DB](https://cbb.pnnl.gov/portal/software/FOAM.html)
- Added support for [Resfams DB](http://www.dantaslab.org/resfams)
- Added support for [CAZy/canDB](http://bcb.unl.edu/dbCAN2/download/Databases/)
