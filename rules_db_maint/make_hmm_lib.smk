rule make_hmm_lib:
  input:
    "{hmmlib}"
  output:
    "{hmmlib}_hmmer-{vers}",
    "{hmmlib}_hmmer-{vers}.h3f",
    "{hmmlib}_hmmer-{vers}.h3i",
    "{hmmlib}_hmmer-{vers}.h3m",
    "{hmmlib}_hmmer-{vers}.h3p"
  message:
    "preparing hmm db for first use: " + os.path.basename("{input[0]}")
  log:
    "{hmmlib}_hmmer-{vers}.log"
  version:
    "0.11"
  conda:
    "../envs/hmmer-{vers}.yaml"
  threads: 4
  benchmark:
    "{hmmlib}_hmmer-{vers}.h3x.benchmark.txt"
  resources:
    mem_mb=100
  shell:
    '''
	ln -s {input[0]} {output[0]}
    hmmpress {output[0]} >> {log} 2>&1
    '''
