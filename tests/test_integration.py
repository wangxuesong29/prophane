#!/usr/bin/env python3
# -*- coding: utf-8 -*-
__authors__ = ["Henning Schiebenhoefer", "Tobias Marschall", "Marcel Martin", "Johannes Köster"]
__copyright__ = "Copyright 2015, Johannes Köster"
__email__ = "koester@jimmy.harvard.edu"
__license__ = "MIT"

import os
import shutil
from os.path import join
import hashlib
import urllib
from shutil import which
import pytest
from snakemake import snakemake
import pandas as pd

if not which("snakemake"):
    raise Exception("snakemake not in PATH. For testing, install snakemake with "
                    "'pip install -e .'. You should do this in a separate environment "
                    "(via conda or virtualenv).")


PROPHANE_DIR = os.path.dirname(os.path.dirname(__file__))


def dpath(path):
    """
    get path to a data file (relative to the directory this
    test lives in)
    """
    return os.path.realpath(join(os.path.dirname(__file__), path))


def md5sum(filename):
    data = open(filename, 'rb').read()
    return hashlib.md5(data).hexdigest()


# test skipping
def is_connected():
    try:
        urllib.request.urlopen("http://www.google.com", timeout=1)
        return True
    except urllib.request.URLError:
        return False


def is_ci():
    return "CI" in os.environ


def has_gcloud_service_key():
    return "GCLOUD_SERVICE_KEY" in os.environ


def has_gcloud_cluster():
    return "GCLOUD_CLUSTER" in os.environ


gcloud = pytest.mark.skipif(not is_connected()
                            or not has_gcloud_service_key()
                            or not has_gcloud_cluster(),
                            reason="Skipping GCLOUD tests because not on "
                                   "CI, no inet connection or not logged "
                                   "in to gcloud.")

connected = pytest.mark.skipif(not is_connected(), reason="no internet connection")

ci = pytest.mark.skipif(not is_ci(), reason="not in CI")


def copy(src, dst):
    if os.path.isdir(src):
        shutil.copytree(src, os.path.join(dst, os.path.basename(src)))
    else:
        shutil.copy(src, dst)


# def test_db_preparation(tmpdir):
#     prophane_test_dbs(tmpdir)
#     src_files = set([x.split('test_dbs/')[1] for x in glob.glob("test_dbs/**/*")])
#     dest_files = set([x.split('test_dbs/')[1] for x in glob.glob(str(tmpdir) + '/test_dbs/**/*')])
#     assert src_files == dest_files


def run_prophane(
        path,
        tmpdir,
        shouldfail=False,
        no_tmpdir=False,
        check_md5=True,
        cores=3,
        check_for_expected_results=True,
        **params
):
    """
    Test the Prophane on 'input' folder in path.
    There must be a subdirectory named
    expected-results in the path.
    """
    expected_results_dir = join(os.path.dirname(__file__), path, 'expected-results')
    snakefile = join(PROPHANE_DIR, 'Snakefile')
    assert os.path.exists(snakefile)
    assert os.path.exists(expected_results_dir) and os.path.isdir(
        expected_results_dir), '{} does not exist'.format(expected_results_dir)

    # run snakemake
    success = snakemake(snakefile,
                        cores=cores, use_conda=True,
                        workdir=path if no_tmpdir else tmpdir,
                        stats="stats.txt",
                        **params)
    if shouldfail:
        assert not success, "expected error on execution"
    else:
        assert success, "expected successful execution"
        if check_for_expected_results:
            compare_with_expected_results(tmpdir, expected_results_dir, check_md5)


def assert_dataframes_equal(df1, df2):
    pd.testing.assert_frame_equal(df1, df2, check_dtype=False)


def assert_summary_content_equal(file1, file2):
    def read_summary(f):
        return pd.read_csv(f, sep="\t")
    dfs_ready_for_comparison = []
    for file in [file1, file2]:
        df = read_summary(file)
        # sort members
        df_groups = df[df['level'] == 'group'].set_index('#pg')
        pg_dict = df_groups['members_identifier'].to_dict()
        # group by protein group members
        groups = df.groupby(lambda x: pg_dict[df.loc[x, '#pg']])
        df_sorted = pd.DataFrame()
        # sort each protein group
        for pg_str, df_pg in groups:
            df_pg = df_pg.sort_values(['level', 'members_identifier'])
            df_sorted = df_sorted.append(df_pg)
        df = df_sorted
        # drop protein group number column, as this is not reproducible at the moment
        df = df.drop(columns="#pg")
        # reindex rows
        df = df.reset_index(drop=True)
        dfs_ready_for_comparison.append(df)
    try:
        assert_dataframes_equal(*dfs_ready_for_comparison)
    except AssertionError as e:
        raise AssertionError(
            f"""
summary.txt files are not equal!
left file:\t{file1}
right file:\t{file2}

File comparison result (first unequal column):
{e}
            """
        )


def test_assert_summary_content_equal():
    # equal summaries of different order
    f1, f2 = [join(os.path.dirname(__file__), "resources/test_is_summary_content_equal/equal", f)
              for f in ["summary1.txt", "summary2.txt"]]
    assert_summary_content_equal(f1, f2)    # , 'summary files [{}, {}] are equal'.format(f1, f2)


def test_assert_summary_content_not_equal():
    # unequal summaries
    f1, f2 = [join(os.path.dirname(__file__), "resources/test_is_summary_content_equal/unequal", f)
              for f in ["summary1.txt", "summary2.txt"]]
    with pytest.raises(AssertionError):
        assert_summary_content_equal(f1, f2)    # , 'summary files [{}, {}] are equal'.format(f1, f2)


def get_files_in_dir(expected_results_dir):
    lst_f = []
    for current_loop_dir, subdirs, list_expected_files in os.walk(expected_results_dir):
        for expected_filename in list_expected_files:
            expected_filepath = join(current_loop_dir, expected_filename)
            lst_f.append(expected_filepath)
    return lst_f


def compare_with_expected_results(result_dir, expected_results_dir, check_md5=True):
    lst_expected_files = get_files_in_dir(expected_results_dir)
    for expected_filepath in lst_expected_files:
        if os.path.basename(expected_filepath) == ".gitignore":
            continue
        rel_file_path = os.path.relpath(expected_filepath, expected_results_dir)
        produced_filepath = os.path.abspath(join(result_dir, rel_file_path))
        assert os.path.exists(
            produced_filepath), 'expected file "{}" not produced'.format(
            rel_file_path)
        if os.path.basename(expected_filepath) == "summary.txt":
            assert_summary_content_equal(expected_filepath, produced_filepath)
        elif check_md5:
            assert md5sum(produced_filepath) == md5sum(expected_filepath), \
                'wrong result produced for file "{}"'.format(rel_file_path)


def test_full_analysis_dryrun(prophane_test_dbs, prophane_input, tmpdir):
    test_ressource_dir = 'resources/test_full_analysis_and_file_presence'
    config_file, job_dir = prophane_input(test_ressource_dir, str(tmpdir))
    run_prophane(test_ressource_dir, str(job_dir), configfile=config_file, check_for_expected_results=False,
                 dryrun=True)


def test_full_analysis_dryrun_wo_sample_groups(prophane_test_dbs, prophane_input, tmpdir):
    test_resource_dir = 'resources/test_full_analysis_dryrun_wo_sample_groups'
    config_file, job_dir = prophane_input(test_resource_dir, str(tmpdir))
    run_prophane(test_resource_dir, str(job_dir), configfile=config_file, check_md5=True,
                 dryrun=True)


def test_full_analysis_and_file_presence(prophane_test_dbs, prophane_input, tmpdir):
    test_ressource_dir = 'resources/test_full_analysis_and_file_presence'
    config_file, job_dir = prophane_input(test_ressource_dir, str(tmpdir))
    run_prophane(test_ressource_dir, str(job_dir), configfile=config_file, check_md5=False)


def test_seqs_all_faa_from_generic_table(prophane_test_dbs, prophane_input, tmpdir):
    test_ressource_dir = 'resources/test_seqs_all_faa_from_generic_table'
    config_file, job_dir = prophane_input(test_ressource_dir, str(tmpdir))
    run_prophane(test_ressource_dir, str(job_dir), configfile=config_file, check_md5=False,
                 targets=['seqs/all.faa'], printshellcmds=True)


def test_parse_generic_table(f_proteomic_search_result, tmpdir):
    test_ressource_dir = os.path.abspath('tests/resources/test_parse_generic_table')
    expected_results_dir = os.path.join(test_ressource_dir, 'expected-results')
    decoy_exp = 'DECOY$'
    style = os.path.abspath('styles/generic.yaml')
    input_table, job_dir = f_proteomic_search_result(test_ressource_dir, tmpdir)
    output_yaml = os.path.join(job_dir, 'protein_groups.yaml')
    required_sample_descriptors = ["sample B::replicate 1", "sample A::replicate 1"]

    from utils.search_result_parsing import ProteomicSearchResultParser
    protein_parser = ProteomicSearchResultParser(input_table, style, output_yaml, decoy_exp, required_sample_descriptors)
    protein_parser.write_protein_groups_yaml()
    compare_with_expected_results(job_dir, expected_results_dir)


def test_pg_yaml_from_scaffold(tmpdir, f_proteomic_search_result):
    test_ressource_dir = os.path.abspath('tests/resources/test_parse_scaffold_table')
    expected_results_dir = os.path.join(test_ressource_dir, 'expected-results')
    decoy_exp = 'DECOY$'
    style = os.path.abspath('styles/scaffold_4_8_x.yaml')
    input_table, job_dir = f_proteomic_search_result(test_ressource_dir, tmpdir)
    output_yaml = os.path.join(job_dir, 'protein_groups.yaml')
    required_sample_descriptors = ["sample A::replicate 1"]

    from utils.search_result_parsing import ProteomicSearchResultParser
    protein_parser = ProteomicSearchResultParser(input_table, style, output_yaml, decoy_exp, required_sample_descriptors)
    protein_parser.write_protein_groups_yaml()
    compare_with_expected_results(job_dir, expected_results_dir)


def test_pg_yaml_from_mpa_portable(tmpdir, f_proteomic_search_result):
    test_ressource_dir = 'tests/resources/test_parse_MPA_portable_table'
    required_sample_descriptors = ["sample1"]
    parse_pg_yaml_from_mpa_table_and_compare_with_expected_yaml(test_ressource_dir, f_proteomic_search_result, tmpdir,
                                                                required_sample_descriptors)


def test_pg_yaml_from_mpa_server(tmpdir, f_proteomic_search_result):
    test_ressource_dir = 'tests/resources/test_parse_MPA_server_table'
    required_sample_descriptors = ["sample1"]
    parse_pg_yaml_from_mpa_table_and_compare_with_expected_yaml(test_ressource_dir, f_proteomic_search_result, tmpdir,
                                                                required_sample_descriptors)


def parse_pg_yaml_from_mpa_table_and_compare_with_expected_yaml(
        test_ressource_dir, f_proteomic_search_result, tmpdir, required_sample_descriptors
):
    test_ressource_dir = os.path.abspath(test_ressource_dir)
    expected_results_dir = os.path.join(test_ressource_dir, 'expected-results')
    decoy_exp = 'DECOY$'
    style = os.path.abspath('styles/mpa_1_8_x.yaml')
    input_table, job_dir = f_proteomic_search_result(test_ressource_dir, tmpdir)
    output_yaml = os.path.join(job_dir, 'protein_groups.yaml')
    from utils.search_result_parsing import ProteomicSearchResultParser
    protein_parser = ProteomicSearchResultParser(input_table, style, output_yaml, decoy_exp, required_sample_descriptors)
    protein_parser.write_protein_groups_yaml()
    compare_with_expected_results(job_dir, expected_results_dir)


def test_pg_yaml_from_mpa_server_multisample_table(tmpdir, f_proteomic_search_result):
    test_ressource_dir = 'tests/resources/test_parse_MPA_server_multisample_table'
    required_sample_descriptors = ["S13.mgf", "S20.mgf", "S23.mgf", "S6.mgf", "S7.mgf", "S25.mgf", "S27.mgf", "S28.mgf",
                                   "S34.mgf", "S36.mgf", "S44.mgf", "S51.mgf", "S61.mgf", "S65.mgf", "S72.mgf"]
    test_ressource_dir = os.path.abspath(test_ressource_dir)
    expected_results_dir = os.path.join(test_ressource_dir, 'expected-results')
    decoy_exp = 'DECOY$'
    style = os.path.abspath('styles/mpa_server_multisample.yaml')
    input_table, job_dir = f_proteomic_search_result(test_ressource_dir, tmpdir)
    output_yaml = os.path.join(job_dir, 'protein_groups.yaml')
    from utils.search_result_parsing.parse_mpa_multisample_metaprotein_report import MpaPortableMultisampleTableReader
    from utils.search_result_parsing import ProteomicSearchResultParser
    parser = ProteomicSearchResultParser(input_table, style, output_yaml, decoy_exp, required_sample_descriptors)
    parser.write_protein_groups_yaml()
    compare_with_expected_results(job_dir, expected_results_dir)


def test_full_analysis_and_file_presence_with_map_style_migration(
        prophane_test_dbs_before_map_gz, prophane_input, tmpdir
):
    test_dbs = prophane_test_dbs_before_map_gz
    from utils.db_handling.migrate_database_schema_version import migrate_dbs_style_if_necessary
    migrate_dbs_style_if_necessary(test_dbs)
    test_ressource_dir = 'resources/test_full_analysis_and_file_presence'
    config_file, job_dir = prophane_input(test_ressource_dir, str(tmpdir), db_dir=test_dbs)
    run_prophane(test_ressource_dir, str(job_dir), configfile=config_file, check_md5=False)
